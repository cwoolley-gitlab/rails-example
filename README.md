# README

This project is focused on exploring the following technologies (and more) in the context of a modern Rails project:

- https://github.com/rubyatscale
- https://products.arkency.com/domain-driven-rails/
- https://hotwired.dev/
- https://semaphoreci.com/blog/ruby-rbs-typeprof-steep
