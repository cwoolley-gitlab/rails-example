// noinspection NpmUsedModulesInstalled
import { Controller } from "@hotwired/stimulus"

// noinspection JSUnusedGlobalSymbols
export default class extends Controller {
  connect() {
    // noinspection JSUnresolvedReference
    this.element.textContent = "Hello World!"
  }
}
